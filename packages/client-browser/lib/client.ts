import * as Core from '@api-client/core';

export class Client implements Core.HttpClient {
  async delete<R>(url: string, token: string): Promise<R> {
    const rawResponse = await fetch(url, {
      method: 'DELETE',
      headers: {
        Accept: 'application/json',
        'content-type': 'application/json',
        Authorization: `Basic ${btoa(token)}`
      }
    });

    return await rawResponse.json();
  }

  async put<B, R>(url: string, body: B, token: string): Promise<R> {
    const rawResponse = await fetch(url, {
      method: 'PUT',
      headers: {
        Accept: 'application/json',
        'content-type': 'application/json',
        Authorization: `Basic ${btoa(token)}`
      },
      body: JSON.stringify(body)
    });

    return await rawResponse.json();
  }

  async post<B, R>(
    url: string,
    body: B,
    token?: string | undefined
  ): Promise<R> {
    const headers: { [index: string]: string } = {
      'content-type': 'application/json',
      Accept: 'application/json'
    };
    if (token) {
      headers['Authorization'] = `Basic ${Buffer.from(token).toString(
        'base64'
      )}`;
    }

    const rawResponse = await fetch(url, {
      method: 'POST',
      headers,
      body: JSON.stringify(body)
    });

    return await rawResponse.json();
  }

  async get<R>(url: string, token: string): Promise<R> {
    const rawResponse = await fetch(url, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'content-type': 'application/json',
        Authorization: `Basic ${btoa(token)}`
      }
    });

    return await rawResponse.json();
  }
}

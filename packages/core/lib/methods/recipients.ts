import {
  ApiVersion,
  DeleteEnqueuedRecipient,
  MultipleRecipientV1,
  MultipleRecipientV2,
  SingleRecipientRecipientV1,
  SingleRecipientRecipientV2,
  SuccessEnqueueMultipleRecipientV1,
  SuccessEnqueueMultipleRecipientV2,
  SuccessEnqueueMultipleRecipientV210,
  SuccessEnqueueOneRecipient
} from '../entities';
import { HttpClient, ResponseByApiVersion } from './helper';

export class Recipients {
  constructor(private httpClient: HttpClient, private baseUrl: string) {}

  enqueueMultipleRecipients<
    version extends ApiVersion.v1 | ApiVersion.v210,
    request extends MultipleRecipientV1 | MultipleRecipientV2,
    response extends ResponseByApiVersion<
      version,
      SuccessEnqueueMultipleRecipientV1,
      SuccessEnqueueMultipleRecipientV210
    >
  >(
    version: version,
    token: string,
    id: string,
    request: request
  ): Promise<response> {
    const endpoint = `automations/${id}/recipients`;

    return this.httpClient.post<request, response>(
      `${this.baseUrl}/${version}/${endpoint}`,
      request,
      token
    );
  }

  enqueueOneRecipient<
    version extends ApiVersion.v1 | ApiVersion.v2,
    request extends SingleRecipientRecipientV1 | SingleRecipientRecipientV2
  >(
    version: version,
    token: string,
    id: string,
    request: request
  ): Promise<SuccessEnqueueOneRecipient> {
    const endpoint = `automations/${id}/recipient`;

    return this.httpClient.post<request, SuccessEnqueueOneRecipient>(
      `${this.baseUrl}/${version}/${endpoint}`,
      request,
      token
    );
  }

  deleteEnqueuedRecipient(
    version: ApiVersion.v1,
    token: string,
    id: string,
    recipientId: string
  ): Promise<DeleteEnqueuedRecipient> {
    const endpoint = `automations/${id}/recipients/${recipientId}`;

    return this.httpClient.delete<DeleteEnqueuedRecipient>(
      `${this.baseUrl}/${version}/${endpoint}`,
      token
    );
  }

  updateEnqueuedRecipient<
    version extends ApiVersion.v1 | ApiVersion.v2,
    request extends SingleRecipientRecipientV1 | SingleRecipientRecipientV2,
    response extends ResponseByApiVersion<
      version,
      SuccessEnqueueMultipleRecipientV1,
      SuccessEnqueueMultipleRecipientV2
    >
  >(
    version: version,
    token: string,
    id: string,
    request: request
  ): Promise<response> {
    const endpoint = `automations/${id}/recipients`;

    return this.httpClient.post<request, response>(
      `${this.baseUrl}/${version}/${endpoint}`,
      request,
      token
    );
  }
}

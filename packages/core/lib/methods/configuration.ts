import { ApiVersion, SystemMappableField } from '../entities';
import { HttpClient } from './helper';

export class Configuration {
  constructor(private httpClient: HttpClient, private baseUrl: string) {}

  getListOfSystemMappableFields(token: string): Promise<SystemMappableField> {
    const endpoint = 'dataMappingFields';

    return this.httpClient.get<SystemMappableField>(
      `${this.baseUrl}/${ApiVersion.v1}/${endpoint}`,
      token
    );
  }
}

import {
  ApiVersion,
  AuthenticateRequest,
  AuthenticateResponse
} from '../entities';
import { HttpClient } from './helper';

export class Authenticate {
  constructor(private httpClient: HttpClient, private baseUrl: string) {}

  generateAuthToken(key: string) {
    const endpoint = 'authenticate';

    return this.httpClient.post<AuthenticateRequest, AuthenticateResponse>(
      `${this.baseUrl}/${ApiVersion.v1}/${endpoint}`,
      {
        key
      }
    );
  }
}

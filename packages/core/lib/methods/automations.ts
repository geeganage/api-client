import {
  ApiVersion,
  AutomationV1,
  AutomationV2,
  CampaignV1,
  CampaignV2,
  CollectedRecipientsCount,
  VisualDataV1,
  VisualDataV2
} from '../entities';
import { HttpClient, ResponseByApiVersion } from './helper';

export class Automations {
  constructor(private httpClient: HttpClient, private baseUrl: string) {}

  getAllAutomations<
    version extends ApiVersion.v1 | ApiVersion.v2,
    response extends ResponseByApiVersion<version, AutomationV1, AutomationV2>
  >(version: version, token: string, query?: string): Promise<response[]> {
    const endpoint = 'automations';

    return this.httpClient.get<response[]>(
      `${this.baseUrl}/${version}/${endpoint}?${query}`,
      token
    );
  }

  getAutomationData<
    version extends ApiVersion.v1 | ApiVersion.v2,
    response extends ResponseByApiVersion<version, AutomationV1, AutomationV2>
  >(version: version, token: string, id: string): Promise<response> {
    const endpoint = `automations/${id}`;

    return this.httpClient.get<response>(
      `${this.baseUrl}/${version}/${endpoint}`,
      token
    );
  }

  getAllCampaigns<
    version extends ApiVersion.v1 | ApiVersion.v2,
    response extends ResponseByApiVersion<version, CampaignV1, CampaignV2>
  >(
    version: version,
    token: string,
    id: string,
    query?: string
  ): Promise<response[]> {
    const endpoint = `automations/${id}/campaigns`;

    return this.httpClient.get<response[]>(
      `${this.baseUrl}/${version}/${endpoint}?${query}`,
      token
    );
  }

  getCampaignData<
    version extends ApiVersion.v1 | ApiVersion.v2,
    response extends ResponseByApiVersion<version, CampaignV1, CampaignV2>
  >(
    version: version,
    token: string,
    id: string,
    campaignId: string
  ): Promise<response> {
    const endpoint = `automations/${id}/campaigns/${campaignId}`;

    return this.httpClient.get<response>(
      `${this.baseUrl}/${version}/${endpoint}`,
      token
    );
  }

  getAllVisuals<
    version extends ApiVersion.v1 | ApiVersion.v2,
    response extends ResponseByApiVersion<version, VisualDataV1, VisualDataV2>
  >(version: version, token: string, id: string): Promise<response[]> {
    const endpoint = `automations/${id}/visuals`;

    return this.httpClient.get<response[]>(
      `${this.baseUrl}/${version}/${endpoint}`,
      token
    );
  }

  getVisualData<
    version extends ApiVersion.v1 | ApiVersion.v2,
    response extends ResponseByApiVersion<version, VisualDataV1, VisualDataV2>
  >(
    version: version,
    token: string,
    id: string,
    visualId: string
  ): Promise<response[]> {
    const endpoint = `automations/${id}/visuals/${visualId}`;

    return this.httpClient.get<response[]>(
      `${this.baseUrl}/${version}/${endpoint}`,
      token
    );
  }

  getNumOfCollectedAddresses<version extends ApiVersion.v1 | ApiVersion.v2>(
    version: version,
    token: string,
    id: string
  ): Promise<CollectedRecipientsCount> {
    const endpoint = `automations/${id}/collectedRecipientsCount`;

    return this.httpClient.get<CollectedRecipientsCount>(
      `${this.baseUrl}/${version}/${endpoint}`,
      token
    );
  }
}

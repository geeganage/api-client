import { ApiVersion } from '../entities';

export type ResponseByApiVersion<
  ApiV extends ApiVersion,
  ModelForV1,
  ModelForV2,
  ModelForV21 = undefined
> = ApiV extends ApiVersion.v1
  ? ModelForV1
  : ApiV extends ApiVersion.v2
  ? ModelForV2
  : ApiV extends ApiVersion.v210
  ? ModelForV21
  : never;

export interface HttpClient {
  delete<R>(url: string, token: string): Promise<R>;
  put<B, R>(url: string, body: B, token: string): Promise<R>;
  post<B, R>(url: string, body: B, token?: string): Promise<R>;
  get<R>(url: string, token: string): Promise<R>;
}

import * as Entities from '../entities';
import * as Methods from '../methods';

export class V210 {
  readonly recipientsV210: RecipientsV210;
  constructor(private readonly recipientsMethods: Methods.Recipients) {
    this.recipientsV210 = new RecipientsV210(this.recipientsMethods);
  }

  get recipients() {
    return this.recipientsV210;
  }
}

class RecipientsV210 {
  constructor(private readonly recipientsMethods: Methods.Recipients) {}

  enqueueMultipleRecipients(
    token: string,
    id: string,
    request: Entities.MultipleRecipientV2
  ) {
    return this.recipientsMethods.enqueueMultipleRecipients<
      Entities.ApiVersion.v210,
      Entities.MultipleRecipientV2,
      Entities.SuccessEnqueueMultipleRecipientV210
    >(Entities.ApiVersion.v210, token, id, request);
  }
}

import * as Methods from '../methods';

export class Common {
  private readonly configurationCommon: ConfigurationCommon;
  private readonly authenticateCommon: AuthenticateCommon;
  constructor(
    private readonly authenticateMethods: Methods.Authenticate,
    private readonly configurationMethods: Methods.Configuration
  ) {
    this.authenticateCommon = new AuthenticateCommon(this.authenticateMethods);

    this.configurationCommon = new ConfigurationCommon(
      this.configurationMethods
    );
  }

  get configuration() {
    return this.configurationCommon;
  }

  get authenticate() {
    return this.authenticateCommon;
  }
}

class ConfigurationCommon {
  constructor(private readonly configurationMethods: Methods.Configuration) {}
  getListOfSystemMappableFields(token: string) {
    return this.configurationMethods.getListOfSystemMappableFields(token);
  }
}

class AuthenticateCommon {
  constructor(private readonly authenticateMethods: Methods.Authenticate) {}

  generateAuthToken(key: string) {
    return this.authenticateMethods.generateAuthToken(key);
  }
}

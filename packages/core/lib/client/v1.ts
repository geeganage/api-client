import * as Entities from '../entities';
import * as Methods from '../methods';

export class V1 {
  private readonly automationsV1: AutomationsV1;
  private readonly recipientsV1: RecipientsV1;
  constructor(
    private readonly automationsMethods: Methods.Automations,
    private readonly recipientsMethods: Methods.Recipients
  ) {
    this.automationsV1 = new AutomationsV1(this.automationsMethods);
    this.recipientsV1 = new RecipientsV1(this.recipientsMethods);
  }

  get automations() {
    return this.automationsV1;
  }
  get recipients() {
    return this.recipientsV1;
  }
}

class AutomationsV1 {
  constructor(private readonly automationsMethods: Methods.Automations) {}

  getAllAutomations(token: string, query?: string) {
    return this.automationsMethods.getAllAutomations<
      Entities.ApiVersion.v1,
      Entities.AutomationV1
    >(Entities.ApiVersion.v1, token, query);
  }

  getAutomationData(token: string, id: string) {
    return this.automationsMethods.getAutomationData<
      Entities.ApiVersion.v1,
      Entities.AutomationV1
    >(Entities.ApiVersion.v1, token, id);
  }

  getAllCampaigns(token: string, id: string, query?: string) {
    return this.automationsMethods.getAllCampaigns<
      Entities.ApiVersion.v1,
      Entities.CampaignV1
    >(Entities.ApiVersion.v1, token, id, query);
  }

  getCampaignData(token: string, id: string, campaignId: string) {
    return this.automationsMethods.getCampaignData<
      Entities.ApiVersion.v1,
      Entities.CampaignV1
    >(Entities.ApiVersion.v1, token, id, campaignId);
  }

  getAllVisuals(token: string, id: string) {
    return this.automationsMethods.getAllVisuals<
      Entities.ApiVersion.v1,
      Entities.VisualDataV1
    >(Entities.ApiVersion.v1, token, id);
  }

  getVisualData(token: string, id: string, visualId: string) {
    return this.automationsMethods.getVisualData<
      Entities.ApiVersion.v1,
      Entities.VisualDataV1
    >(Entities.ApiVersion.v1, token, id, visualId);
  }

  getNumOfCollectedAddresses(token: string, id: string) {
    return this.automationsMethods.getNumOfCollectedAddresses<
      Entities.ApiVersion.v1
    >(Entities.ApiVersion.v1, token, id);
  }
}

class RecipientsV1 {
  constructor(private readonly recipientsMethods: Methods.Recipients) {}

  enqueueMultipleRecipients(
    token: string,
    id: string,
    request: Entities.MultipleRecipientV1
  ) {
    return this.recipientsMethods.enqueueMultipleRecipients<
      Entities.ApiVersion.v1,
      Entities.MultipleRecipientV1,
      Entities.SuccessEnqueueMultipleRecipientV1
    >(Entities.ApiVersion.v1, token, id, request);
  }

  enqueueOneRecipient(
    token: string,
    id: string,
    request: Entities.SingleRecipientRecipientV1
  ) {
    return this.recipientsMethods.enqueueOneRecipient<
      Entities.ApiVersion.v1,
      Entities.SingleRecipientRecipientV1
    >(Entities.ApiVersion.v1, token, id, request);
  }

  deleteEnqueuedRecipient(token: string, id: string, recipientId: string) {
    return this.recipientsMethods.deleteEnqueuedRecipient(
      Entities.ApiVersion.v1,
      token,
      id,
      recipientId
    );
  }

  updateEnqueuedRecipient(
    token: string,
    id: string,
    request: Entities.SingleRecipientRecipientV1
  ) {
    return this.recipientsMethods.updateEnqueuedRecipient<
      Entities.ApiVersion.v1,
      Entities.SingleRecipientRecipientV1,
      Entities.SuccessEnqueueMultipleRecipientV1
    >(Entities.ApiVersion.v1, token, id, request);
  }
}

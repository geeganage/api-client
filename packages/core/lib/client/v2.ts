import * as Entities from '../entities';
import * as Methods from '../methods';

export class V2 {
  private readonly automationsV2: AutomationsV2;
  private readonly recipientsV2: RecipientsV2;
  constructor(
    private readonly automationsMethods: Methods.Automations,
    private readonly recipientsMethods: Methods.Recipients
  ) {
    this.automationsV2 = new AutomationsV2(this.automationsMethods);
    this.recipientsV2 = new RecipientsV2(this.recipientsMethods);
  }

  get automations() {
    return this.automationsV2;
  }
  get recipients() {
    return this.recipientsV2;
  }
}

class AutomationsV2 {
  constructor(private readonly automationsMethods: Methods.Automations) {}

  getAllAutomations(token: string, query?: string) {
    return this.automationsMethods.getAllAutomations<
      Entities.ApiVersion.v2,
      Entities.AutomationV2
    >(Entities.ApiVersion.v2, token, query);
  }

  getAutomationData(token: string, id: string) {
    return this.automationsMethods.getAutomationData<
      Entities.ApiVersion.v2,
      Entities.AutomationV2
    >(Entities.ApiVersion.v2, token, id);
  }

  getAllCampaigns(token: string, id: string, query?: string) {
    return this.automationsMethods.getAllCampaigns<
      Entities.ApiVersion.v2,
      Entities.CampaignV2
    >(Entities.ApiVersion.v2, token, id, query);
  }

  getCampaignData(token: string, id: string, campaignId: string) {
    return this.automationsMethods.getCampaignData<
      Entities.ApiVersion.v2,
      Entities.CampaignV2
    >(Entities.ApiVersion.v2, token, id, campaignId);
  }

  getAllVisuals(token: string, id: string) {
    return this.automationsMethods.getAllVisuals<
      Entities.ApiVersion.v2,
      Entities.VisualDataV2
    >(Entities.ApiVersion.v2, token, id);
  }

  getVisualData(token: string, id: string, visualId: string) {
    return this.automationsMethods.getVisualData<
      Entities.ApiVersion.v2,
      Entities.VisualDataV2
    >(Entities.ApiVersion.v2, token, id, visualId);
  }

  getNumOfCollectedAddresses(token: string, id: string) {
    return this.automationsMethods.getNumOfCollectedAddresses<
      Entities.ApiVersion.v2
    >(Entities.ApiVersion.v2, token, id);
  }
}

class RecipientsV2 {
  constructor(private readonly recipientsMethods: Methods.Recipients) {}

  enqueueOneRecipient(
    token: string,
    id: string,
    request: Entities.SingleRecipientRecipientV1
  ) {
    return this.recipientsMethods.enqueueOneRecipient<
      Entities.ApiVersion.v2,
      Entities.SingleRecipientRecipientV1
    >(Entities.ApiVersion.v2, token, id, request);
  }

  updateEnqueuedRecipient(
    token: string,
    id: string,
    request: Entities.SingleRecipientRecipientV2
  ) {
    return this.recipientsMethods.updateEnqueuedRecipient<
      Entities.ApiVersion.v2,
      Entities.SingleRecipientRecipientV2,
      Entities.SuccessEnqueueMultipleRecipientV2
    >(Entities.ApiVersion.v2, token, id, request);
  }
}

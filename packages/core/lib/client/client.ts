import * as methods from '../methods';
import { Common } from './common';
import { V1 } from './v1';
import { V2 } from './v2';
import { V210 } from './v210';

export class Client {
  readonly v1: V1;
  readonly v2: V2;
  readonly v210: V210;
  readonly common: Common;

  private readonly baseUrl: string;
  private readonly authenticateMethods: methods.Authenticate;
  private readonly automationsMethods: methods.Automations;
  private readonly configurationMethods: methods.Configuration;
  private readonly recipientsMethods: methods.Recipients;

  static getClient(httpClient: methods.HttpClient, baseUrl: string) {
    return new Client(httpClient, baseUrl);
  }

  constructor(httpClient: methods.HttpClient, baseUrl: string) {
    this.baseUrl = baseUrl.replace(/\/$/, '');
    this.authenticateMethods = new methods.Authenticate(
      httpClient,
      this.baseUrl
    );
    this.automationsMethods = new methods.Automations(httpClient, this.baseUrl);
    this.configurationMethods = new methods.Configuration(
      httpClient,
      this.baseUrl
    );
    this.recipientsMethods = new methods.Recipients(httpClient, this.baseUrl);

    this.v1 = new V1(this.automationsMethods, this.recipientsMethods);
    this.v2 = new V2(this.automationsMethods, this.recipientsMethods);
    this.v210 = new V210(this.recipientsMethods);
    this.common = new Common(
      this.authenticateMethods,
      this.configurationMethods
    );
  }
}

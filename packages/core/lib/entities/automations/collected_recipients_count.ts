export interface CollectedRecipientsCount {
  automation: RecipientsAutomation;
  campaigns: RecipientsCampaign[];
}

export interface RecipientsAutomation {
  _id: string;
  name: string;
  collectedRecipientsCount: number;
}

export interface RecipientsCampaign {
  _id: string;
  state: string;
  createdAt: Date;
  name: string;
  pricing: number;
  currency: string;
  recipientsCount: number;
}

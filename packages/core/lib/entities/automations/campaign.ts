export interface Pricing {
  net: number;
  vat: number;
  gross: number;
  currency: number;
}

export interface CampaignItem {
  name: string;
  variations: CampaignItemVariation[];
}

export interface CampaignItemVariation {
  visual: string;
}

export interface Print {
  visual?: string;
  volume?: number;
  envelope?: string;
  material?: string;
  format?: string;
  method?: string;
}

interface BaseCampaign {
  _id: string;
  createdAt: Date;
  name: string;
  automation: string;
}

export interface CampaignV1 extends BaseCampaign {
  status: string;
  customerAddressesTotal: number;
  pricing: number;
  currency: string;
  prints: Print[];
}

export interface CampaignV2 extends BaseCampaign {
  state: string;
  pricing: Pricing;
  recipientsCount: number;
  items: CampaignItem[];
}

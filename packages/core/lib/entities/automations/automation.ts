export interface AutomationVariation {
  id: number;
  name: string;
  visuals: string[];
}

export interface Voucher {
  addressField: string;
  voucherValue: string;
  voucherCode: string;
  variationId: string;
}

export interface AutomationItem {
  name: string;
  vouchers: Voucher[];
}

export interface CreateDays {
  1?: boolean;
  2?: boolean;
  3?: boolean;
  4?: boolean;
  5?: boolean;
  6?: boolean;
  7?: boolean;
}

export interface Postage {
  createVolume?: number;
  createDays?: CreateDays;
}

export interface PrintOption {
  envelope: string;
  method: string;
  material: string;
  format: string;
  name: string;
}

interface BaseAutomation {
  _id: string;
  state: string;
  createdAt: Date;
  name: string;
}

export interface AutomationV1 extends BaseAutomation {
  currency: string;
  postage: Postage;
  printOptions: PrintOption[];
}

export interface AutomationV2 extends BaseAutomation {
  postage: Postage;
  items: AutomationItem[];
  variations: AutomationVariation[];
}

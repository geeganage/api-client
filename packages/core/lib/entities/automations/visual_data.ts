export interface VisualFile {
  name: string;
  fileUrl: string;
  thumbnails: [];
}

interface BaseVisualData {
  _id: string;
  name: string;
  createdAt: Date;
}

export interface VisualDataV1 extends BaseVisualData {
  files: VisualFile[];
}

export interface VisualDataV2 extends BaseVisualData {
  createdAt: Date;
  frontUrl: string;
  frontThumbUrl: string;
  backUrl: string;
  backThumbUrl: string;
}

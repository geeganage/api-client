export interface SuccessEnqueueOneRecipient {
  id: string;
  totalRecipientsCount: number;
  totalRecipientsCountBefore: number;
}

export interface SuccessEnqueueMultipleRecipientV1 {
  id: string;
  queued: number;
  ignored: number;
  totalRecipientsCount: number;
  totalRecipientsCountBefore: number;
  errors?: {
    message?: string;
    itemIndex?: number;
    data?: object;
  }[];
}

export interface SuccessEnqueueMultipleRecipientV2 {
  ids: string[];
  queued: number;
  ignored: number;
  totalRecipientsCount: number;
  totalRecipientsCountBefore: number;
  errors?: {
    message?: string;
    itemIndex?: string;
  }[];
}

export interface SuccessEnqueueMultipleRecipientV210 {
  requestId: string;
  queued: number;
  ignored: number;
  totalRecipientsCount: number;
  totalRecipientsCountBefore: number;
  results?: {
    id?: string;
    errors?: {
      message?: string;
    }[];
  }[];
}

export interface DeleteEnqueuedRecipient {
  deleted: number;
  totalRecipientsCount: number;
}

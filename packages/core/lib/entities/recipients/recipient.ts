interface RecipientBaseRecipient {
  addresses: RecipientAddress[];
}

export interface RecipientAddress {
  title?: string;
  otherTitles?: string;
  jobTitle?: string;
  gender?: string;
  companyName1?: string;
  companyName2?: string;
  companyName3?: string;
  individualisation1?: string;
  individualisation2?: string;
  individualisation3?: string;
  careOf?: string;
  firstName?: string;
  lastName: string;
  fullName: string;
  houseNumber?: string;
  street: string;
  address1: string;
  address2?: string;
  zipCode: string;
  city: string;
  country?: string;
}

export interface RecipientVoucher {
  code: string;
  name: string;
  notUnique?: boolean;
}

export interface MultipleRecipientV1 extends RecipientBaseRecipient {
  visualId?: string;
  voucherCode: string;
}

export interface MultipleRecipientV2 extends RecipientBaseRecipient {
  variation?: string;
  vouchers?: RecipientVoucher[];
}

interface BaseSingleRecipientRecipient {
  address: RecipientAddress
}

export interface SingleRecipientRecipientV1 extends BaseSingleRecipientRecipient {
  visualId?: string;
  voucherCode: string;
}

export interface SingleRecipientRecipientV2 extends BaseSingleRecipientRecipient {
  variation?: string;
  vouchers?: RecipientVoucher[];
}
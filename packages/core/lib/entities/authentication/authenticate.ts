export interface AuthenticateRequest {
  key: string;
}

export interface AuthenticateResponse {
  token: string;
  validUntil: string;
}

export interface SystemMappableField {
  label: string;
  fieldName: string;
  required: boolean;
  type: SystemMappableFieldType;
}

export enum SystemMappableFieldType {
  string = 'string',
  collection = 'collection'
}

import * as Core from '@api-client/core';
import axios from 'axios';

export class Client implements Core.HttpClient {
  async delete<R>(url: string, token: string): Promise<R> {
    const { data } = await axios.delete<R>(url, {
      responseType: 'json',
      headers: {
        'content-type': 'application/json',
        Authorization: `Basic ${Buffer.from(token).toString('base64')}`
      }
    });

    return data;
  }

  async put<B, R>(url: string, body: B, token: string): Promise<R> {
    const { data } = await axios.put<R>(url, body, {
      responseType: 'json',
      headers: {
        'content-type': 'application/json',
        Authorization: `Basic ${Buffer.from(token).toString('base64')}`
      }
    });

    return data;
  }

  async post<B, R>(
    url: string,
    body: B,
    token?: string | undefined
  ): Promise<R> {
    const headers: { [index: string]: string } = {
      'content-type': 'application/json'
    };
    if (token) {
      headers['Authorization'] = `Basic ${Buffer.from(token).toString(
        'base64'
      )}`;
    }

    const { data } = await axios.post<R>(url, body, {
      responseType: 'json',
      headers
    });

    return data;
  }

  async get<R>(url: string, token: string): Promise<R> {
    const { data } = await axios.get<R>(url, {
      responseType: 'json',
      headers: {
        Authorization: `Basic ${Buffer.from(token).toString('base64')}`
      }
    });

    return data;
  }
}

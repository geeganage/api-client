import * as Core from '@api-client/core';
import { Client } from './lib/client';

export function getClient(baseUrl: string) {
  return Core.Client.getClient(new Client(), baseUrl);
}

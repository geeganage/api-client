import { getClient } from "@api-client/client-node";

async function sampleClient(url: string) {
  const client = getClient(url);
  let token: string = '123';

  try {
    // Common
    const { token: token } = await client.common.authenticate.generateAuthToken(
      "temp-token"
    );
  } catch {}

  try {
    const config = await client.common.configuration.getListOfSystemMappableFields(
      token
    );
  } catch {}

  // V1
  try {
    const allAutomationsV1 = await client.v1.automations.getAllAutomations(
      token
    );
  } catch {}

  try {
    const deleteRecipient = await client.v1.recipients.deleteEnqueuedRecipient(
      token,
      "id",
      "recipientId"
    );
  } catch {}

  // V2
  try {
    const allAutomationsV2 = await client.v2.automations.getAllAutomations(
      token,
      "query"
    );
  } catch {}

  try {
    const updateRecipient = await client.v2.recipients.updateEnqueuedRecipient(
      token,
      "id",
      {
        address: {
          address1: "iii",
          title: "Herr",
          firstName: "Max",
          lastName: "Mustermann",
          street: "Schönhauser Str.",
          houseNumber: "19",
          zipCode: "10178",
          city: "Berlin",
          country: "Deutschland",
          fullName: "fullName"
        },
        variation: "1",
        vouchers: [
          {
            code: "XCODE123",
            name: "voucher1"
          },
          {
            code: "YCODE123",
            name: "voucher2"
          },
          {
            code: "GENERIC-CODE",
            name: "ref-voucher",
            notUnique: true
          }
        ]
      }
    );
  } catch {}

  // V2.1.0
  try {
    const updateRecipientV210 = await client.v210.recipients.enqueueMultipleRecipients(
      token,
      "id",
      {
        addresses: [
          {
            address1: "iii",
            title: "Herr",
            firstName: "Max",
            lastName: "Mustermann",
            street: "Schönhauser Str.",
            houseNumber: "19",
            zipCode: "10178",
            city: "Berlin",
            country: "Deutschland",
            fullName: "fullName"
          }
        ],
        variation: "1",
        vouchers: [
          {
            code: "XCODE123",
            name: "voucher1"
          },
          {
            code: "YCODE123",
            name: "voucher2"
          },
          {
            code: "GENERIC-CODE",
            name: "ref-voucher",
            notUnique: true
          }
        ]
      }
    );
  } catch {}
}

sampleClient("http://ab")
  .then(console.log)
  .catch(console.log);

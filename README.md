### Folder structure
* package
    * `client-browser` - contains api client which can be used in `web browser`
    * `client-node` - contains api client which can be used in `Node environment`
    * `core` - Methods, entities and base client
          * `lib/client` - base client. contains different version methods (`v1`, `v2`, `v2.1.0`)
          * `lib/entities` - Entities based on API spec. Such as `authentication`, `automations` which used for `requests` and `responses`
          * `lib/methods` - Methods based  on API spec
* example
   * Contains a small sample.

### How to use

1. To build please run `make build`

2. Example is located in the `example` folder.
   Goto `example` folder and run `yarn execute`

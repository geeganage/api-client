PACKAGE_FOLDER = ./packages

build-package:
		for dir in $(PACKAGE_FOLDER)/* ; do \
						cd $${dir}; \
						echo $${dir}; \
						yarn build ; \
						cd - > /dev/null; \
		done

clean:
		rm -rf node_modules
		rm -rf */node_modules
		rm -rf */**/node_modules
		rm -rf */**/dist

build:
		$(MAKE) clean
		yarn install
		$(MAKE) build-package

.PHONY: build-package \
build \
clean